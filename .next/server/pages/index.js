"use strict";
/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
(() => {
var exports = {};
exports.id = "pages/index";
exports.ids = ["pages/index"];
exports.modules = {

/***/ "./pages/index.js":
/*!************************!*\
  !*** ./pages/index.js ***!
  \************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ \"react/jsx-dev-runtime\");\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);\n\n/*\r\n * @Author: 遥航\r\n * @Date: 2022-02-16 09:02:38\r\n * @LastEditors: 遥航\r\n * @LastEditTime: 2022-02-16 11:32:50\r\n * @FilePath: \\pages\\index.js\r\n * @Description: \r\n * Copyright 2022 OBKoro1, All Rights Reserved. \r\n * 2022-02-16 09:02:38\r\n */ const IndexPage = ()=>{\n    return(/*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"div\", {\n        className: \"Index\",\n        children: [\n            /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"video\", {\n                width: \"100%\",\n                id: \"video\",\n                \"x-webkit-airplay\": \"true\",\n                airplay: \"allow\",\n                \"webkit-playsinline\": \"true\",\n                src: \"https://ljs1.wmcn.com/static/upload/video/20201026/16037095810705273.mp4\",\n                loop: true,\n                muted: true\n            }, void 0, false, {\n                fileName: \"D:\\\\LearningMaterials\\\\项目实训\\\\项目一考试\\\\App\\\\pages\\\\index.js\",\n                lineNumber: 14,\n                columnNumber: 5\n            }, undefined),\n            /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"h2\", {\n                className: \"Index_Title\",\n                children: [\n                    /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"b\", {\n                        children: \"中国领先的 科技驱动型\"\n                    }, void 0, false, {\n                        fileName: \"D:\\\\LearningMaterials\\\\项目实训\\\\项目一考试\\\\App\\\\pages\\\\index.js\",\n                        lineNumber: 20,\n                        columnNumber: 7\n                    }, undefined),\n                    /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"br\", {}, void 0, false, {\n                        fileName: \"D:\\\\LearningMaterials\\\\项目实训\\\\项目一考试\\\\App\\\\pages\\\\index.js\",\n                        lineNumber: 20,\n                        columnNumber: 25\n                    }, undefined),\n                    /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"b\", {\n                        children: \"个人金融服务平台\"\n                    }, void 0, false, {\n                        fileName: \"D:\\\\LearningMaterials\\\\项目实训\\\\项目一考试\\\\App\\\\pages\\\\index.js\",\n                        lineNumber: 21,\n                        columnNumber: 7\n                    }, undefined)\n                ]\n            }, void 0, true, {\n                fileName: \"D:\\\\LearningMaterials\\\\项目实训\\\\项目一考试\\\\App\\\\pages\\\\index.js\",\n                lineNumber: 19,\n                columnNumber: 5\n            }, undefined)\n        ]\n    }, void 0, true, {\n        fileName: \"D:\\\\LearningMaterials\\\\项目实训\\\\项目一考试\\\\App\\\\pages\\\\index.js\",\n        lineNumber: 13,\n        columnNumber: 10\n    }, undefined));\n};\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (IndexPage);\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9wYWdlcy9pbmRleC5qcy5qcyIsIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUEsRUFTRztJQUdELE1BQU07UUFBTUUsU0FBUyxFQUFDLENBQU87O2tDQUMxQkMsQ0FBSztnQkFBQ0MsS0FBSyxFQUFDLENBQU07Z0JBQUNDLEVBQUU7Z0JBQVNDLENBQWdCLG1CQUFDLENBQU07Z0JBQUNDLE9BQU87Z0JBQzVEQyxDQUFrQjtnQkFDbEJDLEdBQUcsRUFBQyxDQUEwRTtnQkFDOUVDLElBQUk7Z0JBQUNDOzs7Ozs7a0NBRU5DLENBQUU7Z0JBQUNWLFNBQVMsRUFBQyxDQUFhOzs7Ozs7Ozs7Ozs7OzswQ0FFSlcsQ0FBbkI7a0NBQUMsQ0FBUTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBR2pCLENBQUM7QUFHRCxNQUFNLFNBQVNiLFNBQVMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9wYWdlcy9pbmRleC5qcz9iZWU3Il0sInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbiAqIEBBdXRob3I6IOmBpeiIqlxyXG4gKiBARGF0ZTogMjAyMi0wMi0xNiAwOTowMjozOFxyXG4gKiBATGFzdEVkaXRvcnM6IOmBpeiIqlxyXG4gKiBATGFzdEVkaXRUaW1lOiAyMDIyLTAyLTE2IDExOjMyOjUwXHJcbiAqIEBGaWxlUGF0aDogXFxwYWdlc1xcaW5kZXguanNcclxuICogQERlc2NyaXB0aW9uOiBcclxuICogQ29weXJpZ2h0IDIwMjIgT0JLb3JvMSwgQWxsIFJpZ2h0cyBSZXNlcnZlZC4gXHJcbiAqIDIwMjItMDItMTYgMDk6MDI6MzhcclxuICovXHJcblxyXG5jb25zdCBJbmRleFBhZ2UgPSAoKSA9PiB7XHJcbiAgcmV0dXJuIDxkaXYgY2xhc3NOYW1lPVwiSW5kZXhcIj5cclxuICAgIDx2aWRlbyB3aWR0aD1cIjEwMCVcIiBpZD1cInZpZGVvXCIgeC13ZWJraXQtYWlycGxheT1cInRydWVcIiBhaXJwbGF5PVwiYWxsb3dcIlxyXG4gICAgICB3ZWJraXQtcGxheXNpbmxpbmU9XCJ0cnVlXCJcclxuICAgICAgc3JjPVwiaHR0cHM6Ly9sanMxLndtY24uY29tL3N0YXRpYy91cGxvYWQvdmlkZW8vMjAyMDEwMjYvMTYwMzcwOTU4MTA3MDUyNzMubXA0XCJcclxuICAgICAgbG9vcCBtdXRlZFxyXG4gICAgPjwvdmlkZW8+XHJcbiAgICA8aDIgY2xhc3NOYW1lPVwiSW5kZXhfVGl0bGVcIj5cclxuICAgICAgPGI+5Lit5Zu96aKG5YWI55qEIOenkeaKgOmpseWKqOWeizwvYj48YnIgLz5cclxuICAgICAgPGI+5Liq5Lq66YeR6J6N5pyN5Yqh5bmz5Y+wPC9iPlxyXG4gICAgPC9oMj5cclxuICA8L2Rpdj5cclxufVxyXG5cclxuXHJcbmV4cG9ydCBkZWZhdWx0IEluZGV4UGFnZSJdLCJuYW1lcyI6WyJJbmRleFBhZ2UiLCJkaXYiLCJjbGFzc05hbWUiLCJ2aWRlbyIsIndpZHRoIiwiaWQiLCJ4LXdlYmtpdC1haXJwbGF5IiwiYWlycGxheSIsIndlYmtpdC1wbGF5c2lubGluZSIsInNyYyIsImxvb3AiLCJtdXRlZCIsImgyIiwiYiIsImJyIl0sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./pages/index.js\n");

/***/ }),

/***/ "react/jsx-dev-runtime":
/*!****************************************!*\
  !*** external "react/jsx-dev-runtime" ***!
  \****************************************/
/***/ ((module) => {

module.exports = require("react/jsx-dev-runtime");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = (__webpack_exec__("./pages/index.js"));
module.exports = __webpack_exports__;

})();
/*
 * @Author: 遥航
 * @Date: 2022-02-16 09:02:38
 * @LastEditors: 遥航
 * @LastEditTime: 2022-02-16 11:32:50
 * @FilePath: \pages\index.js
 * @Description: 
 * Copyright 2022 OBKoro1, All Rights Reserved. 
 * 2022-02-16 09:02:38
 */

const IndexPage = () => {
  return <div className="Index">
    <video width="100%" id="video" x-webkit-airplay="true" airplay="allow"
      webkit-playsinline="true"
      src="https://ljs1.wmcn.com/static/upload/video/20201026/16037095810705273.mp4"
      loop muted
    ></video>
    <h2 className="Index_Title">
      <b>中国领先的 科技驱动型</b><br />
      <b>个人金融服务平台</b>
    </h2>
  </div>
}


export default IndexPage
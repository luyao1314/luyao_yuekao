/*
 * @Author: 遥航
 * @Date: 2022-02-16 09:16:39
 * @LastEditors: 遥航
 * @LastEditTime: 2022-02-16 11:56:16
 * @FilePath: \pages\_app.js
 * @Description: 
 * Copyright 2022 OBKoro1, All Rights Reserved. 
 * 2022-02-16 09:16:39
 */
import "../styles/app.css";
import 'react-vant/lib/sticky/style/index.css';
import "antd/dist/antd.css";
import { Sticky } from 'react-vant';
import PageHeader from "../components/PageHeader.js"
const MyApp = ({ Component, pageProps }) => {
  return <div className="app">
    <Sticky>
      <PageHeader />
    </Sticky>
    <Component {...pageProps} />
  </div>;
}


export default MyApp

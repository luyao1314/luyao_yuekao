/*
 * @Author: 遥航
 * @Date: 2022-02-16 10:34:32
 * @LastEditors: 遥航
 * @LastEditTime: 2022-02-16 10:46:08
 * @FilePath: \utils\NavData.js
 * @Description: 
 * Copyright 2022 OBKoro1, All Rights Reserved. 
 * 2022-02-16 10:34:32
 */
const NavData = [
  {
    title: "首页",
    path: "/",
  },
  {
    title: "关于我们",
    path: "/company",
    menu: [
      "公司简介",
      "使命与定位",
      "成员公司",
      "管理团队",
      "发展历程",
      "奖项荣誉",
      "合作伙伴",
      "企业文化",
      "关于平安"
    ]
  },
  {
    title: "业务与服务",
    path: "/services",
    menu: [
      "业务概括",
      "我们的产品",
      "我们的服务",
      "我们的技术",
      "科技赋能"
    ]
  },
  {
    title: "新闻中心",
    path: "/news"
  },
  {
    title: "投资者关系",
    path: "https://ir.lufaxholding.com/home/default.aspx"
  },
  {
    title: "联系我们",
    path: "/contactUs"
  },
  {
    title: "加入我们",
    path: "https://talent.pingan.com/recruit/social.html"
  }

]

export default NavData
/*
 * @Author: 遥航
 * @Date: 2022-02-16 09:46:06
 * @LastEditors: 遥航
 * @LastEditTime: 2022-02-16 11:52:23
 * @FilePath: \components\PageHeader.js
 * @Description: 页面导航组件
 * Copyright 2022 OBKoro1, All Rights Reserved. 
 * 2022-02-16 09:46:06
 */
import { Dropdown, Menu } from "antd"
import Link from 'next/link'
import NavData from "../utils/NavData"
const UseMenu = (data, url) => {
  return <Menu>
    {
      data.map((item) => {
        return <Menu.Item key={item}>
          <div>
            <Link href={{ pathname: url, query: { title: item } }}>
              {item}
            </Link>
          </div>
        </Menu.Item>
      })
    }
  </Menu >
}
const PageHeader = () => {
  return <div className="PageHeaderWarp">
    <div className="PageHeader">
      <h1><img src="https://static.lufaxcdn.com/lufaxholding/common/images/logo.80d5324a.jpg" alt="" /></h1>
      <div className="HeaderNav">
        {
          NavData.map((item, index) => {
            if (item.menu) {
              return <Dropdown overlay={UseMenu(item.menu, item.path)} key={"title" + index} placement="bottomCenter">
                <div className="NavLink">
                  <Link href={item.path}>{item.title}</Link>
                </div>
              </Dropdown>
            } else {
              return <div className="NavLink" key={item.title + index}>
                <Link href={item.path}>{item.title}</Link>
              </div>
            }
          })
        }
      </div>
    </div>
  </div>
}
export default PageHeader